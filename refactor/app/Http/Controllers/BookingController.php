<?php

namespace DTApi\Http\Controllers;

use DTApi\Models\Job;
use DTApi\Http\Requests;
use DTApi\Models\Distance;
use Illuminate\Http\Request;
use DTApi\Repository\BookingRepository;
use DTApi\Repository\BookingNotificationRepository as Notification;

/**
 * Class BookingController
 * @package DTApi\Http\Controllers
 */
class BookingController extends Controller
{
    /**
     * @var BookingNotificationRepository
     */
    protected $notification;

    /**
     * @var BookingRepository
     */
    protected $repository;

    /**
     * BookingController constructor.
     * @param BookingRepository $bookingRepository
     */
    public function __construct(BookingRepository $bookingRepository)
    {
        $this->repository = $bookingRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user_type = $request->__authenticatedUser->user_type;
        $user_id = $request->get('user_id');
        if($user_id ) 
        {
            $response = $this->repository->getUsersJobs($user_id);
        }
        elseif($user_type == env('ADMIN_ROLE_ID') || $user_type == env('SUPERADMIN_ROLE_ID'))
        {
            $response = $this->repository->getAll($request);
        }else{
            $response = ['error' => 'Authentication invalid!'];
        }

        return response($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return response(
            $this->repository->with('translatorJobRel.user')->find($id)
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return response(
            $this->repository->store($request->__authenticatedUser, $request->all())
        );
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $data = $request->all();
        $cuser = $request->__authenticatedUser;

        return response(
            $this->repository->updateJob($id, array_except($data, ['_token', 'submit']), $cuser)
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function immediateJobEmail(Request $request)
    {
        $adminSenderEmail = config('app.adminemail');
        $data = $request->all();

        return response(
            $this->repository->storeJobEmail($data)
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getHistory(Request $request)
    {
        if($user_id == $request->get('user_id')) 
        {
            return response( $this->repository->getUsersJobsHistory($user_id, $request) );
        }
        return null;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function acceptJob(Request $request)
    {
        $data = $request->all();
        $user = $request->__authenticatedUser;

        return response(
            $this->repository->acceptJob($data, $user)
        );
    }

    public function acceptJobWithId(Request $request)
    {
        $data = $request->get('job_id');
        $user = $request->__authenticatedUser;

        return response(
            $this->repository->acceptJobWithId($data, $user)
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function cancelJob(Request $request)
    {
        $data = $request->all();
        $user = $request->__authenticatedUser;

        return response(
            $this->repository->cancelJobAjax($data, $user)
        );
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function endJob(Request $request)
    {
        return response(
            $this->repository->endJob( $request->all() )
        );

    }

    public function customerNotCall(Request $request)
    {
        return response(
            $this->repository->customerNotCall( $request->all() )
        );

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPotentialJobs(Request $request)
    {
        return response(
            $this->repository->getPotentialJobs( $request->__authenticatedUser )
        );
    }

    public function distanceFeed(Request $request)
    {
        $data = $request->all();

        $distance =  (isset($data['distance']) && $data['distance'] != "") ?  $data['distance'] : "";
        $time = (isset($data['time']) && $data['time'] != "") ? $data['time'] : "";
        $jobid = (isset($data['jobid']) && $data['jobid'] != "") ? $data['jobid'] : "";
        $session = (isset($data['session_time']) && $data['session_time'] != "") ? $data['session_time'] : "";

        $flagged = 'no';
        if ($data['flagged'] == 'true') {
            if($data['admincomment'] == '') return "Please, add comment";
            $flagged = 'yes';
        }
        
        $manually_handled = ($data['manually_handled'] == 'true')? 'yes' : 'no';
        $by_admin = ($data['by_admin'] == 'true') ? 'yes' : 'no';
        $admincomment = (isset($data['admincomment']) && $data['admincomment'] != "") ? $data['admincomment'] : "";

        if ($time || $distance) {
            $data = array('distance' => $distance, 'time' => $time);
           Distance::where('job_id', '=', $jobid)->update($data);
        }

        if ($admincomment || $session || $flagged || $manually_handled || $by_admin) {
            $data = array(
                'admin_comments' => $admincomment, 
                'flagged' => $flagged, 
                'session_time' => $session, 
                'manually_handled' => $manually_handled, 
                'by_admin' => $by_admin
            );
            Job::where('id', '=', $jobid)->update( $data );
        }

        return response(['success' => 'Record updated!']);
    }

    public function reopen(Request $request)
    {
        return response( 
            $this->repository->reopen( $request->all() )
        );
    }

    public function resendNotifications(Request $request)
    {
        $data = $request->all();
        $job = $this->repository->find($data['jobid']);
        $job_data = $this->repository->jobToData($job);
        $this->notification->sendTranslator($job, $job_data, '*');

        return response(['success' => 'Push sent']);
    }

    /**
     * Sends SMS to Translator
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function resendSMSNotifications(Request $request)
    {
        $data = $request->all();
        $job = $this->repository->find($data['jobid']);
        $job_data = $this->repository->jobToData($job);

        try {
            $this->notification->sendSMSToTranslator($job);
            return response(['success' => 'SMS sent']);
        } catch (\Exception $e) {
            return response(['success' => $e->getMessage()]);
        }
    }
}
